package sample;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

public class Controller extends Main implements Initializable{

    public Button btn7;
    public Button btn8;
    public Button btn9;
    public Button btn4;
    public Button btn5;
    public Button btn6;
    public Button btn3;
    public Button btnK;
    public Button btn0;
    public Button btn00;
    public Button btn2;
    public Button btnSqr;
    public Button btnSqrt;
    public Button btnMnojenn9;
    public Button btnDilenn9;
    public Button btnPlus;
    public Button btnMinus;
    public Button btnL;
    public Button btnC;
    public MenuItem menuExit;
    public MenuItem creator;



    String startText="Введіть число";


    public Button btn1;

    public TextField area;

    @FXML
    public void StartText (){
        if (area.getText().equals(startText))
            area.setText("");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    public void action1(ActionEvent event) {
        StartText();
        area.setText(area.getText() + "1");
    }
    @FXML
    public void action2(ActionEvent event) {
        StartText();
        area.setText(area.getText() + "2");
    }
    @FXML
    public void action3(ActionEvent event) {
        StartText();
        area.setText(area.getText() + "3");
    }
    @FXML
    public void action4(ActionEvent event) {
        StartText();
        area.setText(area.getText() + "4");
    }
    @FXML
    public void action5(ActionEvent event) {
        StartText();
        area.setText(area.getText() + "5");
    }
    @FXML
    public void action6(ActionEvent event) {
        StartText();
        area.setText(area.getText() + "6");
    }
    @FXML
    public void action7(ActionEvent event) {
        StartText();
        area.setText(area.getText() + "7");
    }
    @FXML
    public void action8(ActionEvent event) {
        StartText();
        area.setText(area.getText() + "8");
    }
    @FXML
    public void action9(ActionEvent event) {
        StartText();
        area.setText(area.getText() + "9");
    }
    @FXML
    public void actionKoma(ActionEvent event) {
        StartText();
        area.setText(area.getText() + ".");
    }
    @FXML
    public void action0(ActionEvent event) {
        StartText();
        area.setText(area.getText() + "0");
    }
    @FXML
    public void action00(ActionEvent event) {
        StartText();
        area.setText(area.getText() + "00");
    }


    @FXML
    public void actionSqr(ActionEvent event) {
        StartText();
        area.setText(area.getText()+"^");
        SearcherErrors();
    }
    @FXML
    public void actionSqrt(ActionEvent event) {
        StartText();
        firstValue = Double.valueOf(area.getText());
        area.setText(Math.sqrt(firstValue) + "");
    }
    @FXML
    public void actionMnojenn9(ActionEvent event) {
        StartText();
        area.setText(area.getText()+"*");
        SearcherErrors();
    }
    @FXML
    public void actionDilenn9(ActionEvent event) {
        StartText();
        area.setText(area.getText()+"/");
        SearcherErrors();
    }
    @FXML
    public void actionPlus(ActionEvent event) {
        StartText();
        area.setText(area.getText()+"+");
        SearcherErrors();
    }
    @FXML
    public void actionMinus(ActionEvent event) {
        StartText();
        area.setText(area.getText()+"-");
        SearcherErrors();
    }
    @FXML
    public void actionL(ActionEvent event) {
        SearcherErrors();
    }
    @FXML
    public void actionC(ActionEvent event) {
        area.setText("");
    }

    double firstValue = 0;
    String operation = "+";
    double secondValue =0;

    String[] parts;
    public void SearcherErrors() {
        if (area.getText().contains("+")) {
            parts = area.getText().split(Pattern.quote("+"));

            firstValue = Double.parseDouble(parts[0]);
            secondValue = Double.parseDouble(parts[1]);

            area.setText(String.valueOf(firstValue + secondValue));

            secondValue = 0;
            firstValue = 0;

        }
        if (area.getText().contains("-")) {
            parts = area.getText().split(Pattern.quote("-"));

            firstValue = Double.parseDouble(parts[0]);
            secondValue = Double.parseDouble(parts[1]);

            area.setText(String.valueOf(firstValue - secondValue));

            secondValue = 0;
            firstValue = 0;

        }
        if (area.getText().contains("/")) {
            parts = area.getText().split(Pattern.quote("/"));

            firstValue = Double.parseDouble(parts[0]);
            secondValue = Double.parseDouble(parts[1]);

            area.setText(String.valueOf(firstValue / secondValue));

            secondValue = 0;
            firstValue = 0;
        }
        if (area.getText().contains("*")) {
            parts = area.getText().split(Pattern.quote("*"));

            firstValue = Double.parseDouble(parts[0]);
            secondValue = Double.parseDouble(parts[1]);

            area.setText(String.valueOf(firstValue * secondValue));

            secondValue = 0;
            firstValue = 0;
        }
        if (area.getText().contains("^")) {
            parts = area.getText().split(Pattern.quote("^"));

            firstValue = Double.parseDouble(parts[0]);
            secondValue = Double.parseDouble(parts[1]);

            area.setText((Math.pow(firstValue, secondValue) + ""));

            secondValue = 0;
            firstValue = 0;
        } else {
            if (area.getText().equals(startText)) {
                area.setText(startText);
            } else {
                Error();
            }
        }
    }

        public double Error() {
            int error = 1;
            double vubir = 0;
            while (error == 1) {
                try {
                    vubir = Double.valueOf(area.getText());
                    error = 0;
                } catch (NumberFormatException e) {
                    area.setText("");
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("Не введено чисел.");
                    alert.setContentText("Повторіть спробу після натиснення OK");

                    alert.showAndWait();
                    area.setText(startText);
                    error = 0;
                }
            }
            return vubir;
        }

    public void exit(ActionEvent event) {
        Platform.exit();
        System.exit(0);
    }

    public void infoAboutCreator(ActionEvent event) {


        // Виклик веб сторінок


        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Informatoin");
        alert.setHeaderText("Дивіться в браузер");
        alert.setContentText("Для продовження роботи натисніть OK \nСлід добавити силки,\nале поки ще з цим не розібрався");
        alert.showAndWait();

    }

    class KeyListen implements KeyListener {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER){
                    SearcherErrors();
                }
                if(e.getKeyCode() >= KeyEvent.VK_0 && e.getKeyCode() >= KeyEvent.VK_9){
                    StartText();
                }
                if(e.getKeyCode() >= KeyEvent.VK_NUMPAD0 && e.getKeyCode() >= KeyEvent.VK_NUMPAD9){
                    StartText();
                }
            }

            public void keyReleased(KeyEvent e) {
            }
        }

}

